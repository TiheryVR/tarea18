#include <stdio.h>
#include "menu.h"

int main(int argc, char const *argv[]) {

menu();
  int seleccion=1;
  int deposito=0;
  int donacion=15;
  int saldo=0;
  int retiro=0;
  int saldo_inicial=0;
  printf("Saldo: %d \n", saldo);

  while (seleccion) {
    seleccion = menu();
      switch (seleccion) {

        case 1:
          printf("Cantidad a depositar:");
            scanf("%d",&deposito);
          saldo=saldo+deposito;
          printf("Su saldo disponible es: %d\n",saldo);
          break;
        case 2:
          printf("Saldo disponible: %d\n", saldo);
          printf("Cantidad a retirar:");
            scanf("%d",&retiro);
            if (retiro <= saldo) {
              saldo=saldo-retiro;
              printf("Nuevo saldo disponible: %d\n", saldo);
            }else{
              printf("Fondos insuficientes, digite otra cantidad.\n");
            }
          break;
        case 3:
          if (saldo>=donacion) {
            printf("Gracias por donar.\n");
            saldo=saldo-donacion;
            printf("Nuevo saldo disponible: %d\n",saldo);
          }else{
            printf("Fondos insuficientes.\n");
          }
          break;
        case 4:
          printf("Saliendo...\n");
          return 0;
          break;
        default:
          printf("Opción invalida\n");

    }
  }
}
